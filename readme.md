# Java EE 8 example application

Simple application for finding duplicate values of localization keys by localization value.

Localization is stored in database.

### Default languages is

 - Ukrainian (UK)
 - English (EN)
 - Russian (RU)

### Result record format:

```json
{
  "key": "string",
  "uk": "Українською",
  "en": "English",
  "ru": "Рус..."
}
```

### Result table format: 

| KEU                 | UK         | EN     | RU            |
| ------------------- | :--------- | :----- | :------------ |
| Button.Crate.Label  | Створити   | Create | Создать       |
| Button.Edit.Label   | Редагувати | Edit   | Редактировать |
| Button.Delete.Label | Видалити   | Delete | Удалить       |
