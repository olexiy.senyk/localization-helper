package ua.soo.localization.helper.log;

import org.slf4j.LoggerFactory;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.function.Function;

@LogExecutionTime
@Interceptor
public class LogExecutionTimeInterceptor {

    private static final Function<InvocationContext, Class<?>> TARGET_CLASS_ACCESSOR =
            new TargetClassWithoutProxyAccessor();

    @AroundInvoke
    public Object logExecutionTime(InvocationContext invocationContext) throws Exception {
        final var targetClass = TARGET_CLASS_ACCESSOR.apply(invocationContext);
        final var logger = LoggerFactory.getLogger(targetClass);
        final var targetMethodName = invocationContext.getMethod().getName();
        final var startAt = LocalDateTime.now();

        try {
            return invocationContext.proceed();
        } finally {
            logger.info("[{}.{}] Execution time: {}ms",
                    targetClass.getName(),
                    targetMethodName,
                    ChronoUnit.MILLIS.between(startAt, LocalDateTime.now())
            );
        }
    }
}
