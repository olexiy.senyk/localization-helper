package ua.soo.localization.helper.log;

import javax.interceptor.InvocationContext;
import java.util.function.Function;

public class TargetClassWithoutProxyAccessor implements Function<InvocationContext, Class<?>> {

    @Override
    public Class<?> apply(InvocationContext invocationContext) {
        final Object targetInstance = invocationContext.getTarget();
        Class<?> targetClass = targetInstance.getClass().getSuperclass(); // remove proxy

        return targetClass == Object.class
                ? targetInstance.getClass() // no proxy
                : targetClass;
    }
}
