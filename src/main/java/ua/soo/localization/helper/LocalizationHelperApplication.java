package ua.soo.localization.helper;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/api")
public class LocalizationHelperApplication extends Application {
}
