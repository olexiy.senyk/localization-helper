package ua.soo.localization.helper;

import lombok.Getter;
import lombok.Setter;
import org.primefaces.model.FilterMeta;
import org.primefaces.util.LangUtils;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

@Named
@ViewScoped
public class LocalizationViewController implements Serializable {

    @Inject
    private LocalizationService service;

    @Getter
    private Collection<LocalizationTuple> list;

    @Getter
    @Setter
    private Collection<LocalizationTuple> filteredList;

    @Getter
    private List<FilterMeta> filterBy;

    @PostConstruct
    public void init() {
        list = service.findAll();
        filterBy = new ArrayList<>();
    }

    public boolean globalFilterFunction(Object value, Object filter, Locale locale) {
        String filterText = (filter == null) ? null : filter.toString().trim().toLowerCase();
        if (LangUtils.isValueBlank(filterText)) {
            return true;
        }

        final LocalizationTuple item = (LocalizationTuple) value;
        return item.key.toLowerCase().contains(filterText)
                || item.uk.toLowerCase().contains(filterText)
                || item.en.toLowerCase().contains(filterText)
                || item.ru.toLowerCase().contains(filterText);
    }

    public Collection<LocalizationTuple> findAll() {
        return service.findAll();
    }
}
