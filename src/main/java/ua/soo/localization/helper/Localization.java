package ua.soo.localization.helper;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "md_sys_localization")
@Getter
@Setter
public class Localization implements Serializable {

    private static final long serialVersionUID = 3399102682996081974L;

    @EmbeddedId
    private LocalizationId id;

    @NotBlank
    @Column(length = 1000)
    private String value;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Localization that = (Localization) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
