package ua.soo.localization.helper;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Tuple;
import java.util.Collection;
import java.util.stream.Collectors;

@Stateless
public class LocalizationService {

    @PersistenceContext
    private EntityManager em;

    public Collection<LocalizationTuple> findAll() {
        return em.createNamedQuery("localization.findAllTuples", Tuple.class)
                .getResultStream()
                .map(LocalizationTuple::of)
                .collect(Collectors.toList());
    }

    public Collection<LocalizationTuple> findByValue(String searchString) {
        return em.createNamedQuery("localization.findTuplesByValue", Tuple.class)
                .setParameter("searchString", searchString)
                .getResultStream()
                .map(LocalizationTuple::of)
                .collect(Collectors.toList());
    }
}
