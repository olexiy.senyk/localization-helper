package ua.soo.localization.helper;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public class LocalizationId implements Serializable {

    private static final long serialVersionUID = -4840402177805954638L;

    @NotBlank
    @Size(max = 128)
    @Column(length = 128)
    private String key;

    @NotBlank
    @Size(min = 2, max = 2)
    @Column(length = 2)
    private String locale;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LocalizationId that = (LocalizationId) o;
        return Objects.equals(key, that.key)
                && Objects.equals(locale, that.locale);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, locale);
    }
}
