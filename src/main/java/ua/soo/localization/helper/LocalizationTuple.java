package ua.soo.localization.helper;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import javax.persistence.Tuple;
import java.io.Serializable;

@Getter
@EqualsAndHashCode
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class LocalizationTuple implements Serializable {
    private static final long serialVersionUID = 3337799308748788220L;

    public final String key;
    public final String uk;
    public final String en;
    public final String ru;

    public static LocalizationTuple of(Tuple tuple) {
        return new LocalizationTuple(
                tuple.get("key", String.class),
                tuple.get("uk", String.class),
                tuple.get("en", String.class),
                tuple.get("ru", String.class)
        );
    }
}
