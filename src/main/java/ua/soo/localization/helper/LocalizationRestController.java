package ua.soo.localization.helper;

import ua.soo.localization.helper.log.LogExecutionTime;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.util.Collection;

@LogExecutionTime
@Path("/localization")
public class LocalizationRestController {

    @Context
    private UriInfo uriInfo;

    @Inject
    private LocalizationService service;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<LocalizationTuple> find(@QueryParam("searchString") String searchString) {
        return searchString == null || searchString.isBlank()
                ? service.findAll() : service.findByValue(searchString);
    }
}
