package ua.soo.localization.helper.liquibase;

import liquibase.Liquibase;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.resource.ClassLoaderResourceAccessor;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.sql.DataSource;

@Startup
@Singleton
@TransactionManagement(TransactionManagementType.BEAN)
public class LiquibaseRunner {

    private static final String STAGE = "dev";
    private static final String CHANGELOG_FILE = "META-INF/liquibase/db.changelog.xml";
    private static final String DATA_SOURCE_JNDI_NAME = "java:jboss/datasources/LocalizationHelperDS";

    @Resource(lookup = DATA_SOURCE_JNDI_NAME)
    private DataSource dataSource;

    @PostConstruct
    protected void bootstrap() {
        try {
            final var connection = dataSource.getConnection();
            final var jdbcConnection = new JdbcConnection(connection);
            final var db = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(jdbcConnection);
            final var resourceAccessor = new ClassLoaderResourceAccessor(getClass().getClassLoader());
            try (var liquibase = new Liquibase(CHANGELOG_FILE, resourceAccessor, db)) {
                liquibase.update(STAGE);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
